#! /usr/bin/env python
# -*- coding:utf-8 -*-
# Author: Arne Köhn <arne@arne-koehn.de>
# License: GPLv3 or later
# this parses the output of cdg's printparse and creates statistics
# about violated constraints.
import sys,re, getopt

opts, args = getopt.getopt(sys.argv[1:], "gvm")

gnuplot_style = False
print_variance = False
print_minmax = False

for k,v in opts:
    if k == "-g":
        gnuplot_style = True
    elif k == "-v":
        print_variance = True
    elif k == "-m":
        print_minmax = True

violations = re.compile("Violations:$")
vio = re.compile(": ([\de+.-]*) : ([^:]*)$")
if len(args)>1:
    exit("only one file supported at this time!")
if len(args)==1:
    f = open(args[0])
else:
    f = sys.stdin

line = "XXXXX"
mode = 0 # we are in the uninteresting lines
result = {}


while line != "":
    line = f.readline()
    if violations.search(line):
        mode = 1 # it gets interesting
        continue
    if not mode:
        continue
    if line == "\n": # end of violations (not interesting anymore)
        mode = 0
        continue
    penalty, rule = vio.search(line).groups()
    try:
        result[rule].append(penalty)
    except:
        result[rule]=[penalty]
endres = []
for k,v in result.iteritems():
    l = len(v)
    v_int = map(float,v)
    v_mean = sum(v_int)/l
    endres.append((k,l,v_mean))
endres.sort(cmp = lambda x,y: cmp(y[1],x[1]))

if gnuplot_style:
    for l in endres:
        print l[1], l[2]
else:
    for l in endres:
        print l[0].strip()," | ",  l[1], " | ",  l[2]
