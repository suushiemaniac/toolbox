#! /usr/bin/env python3
# -*- coding:utf-8 -*-
# Authors: Niels Beuck <beuck@informatik.uni-hamburg.de>,
#          Arne Köhn <arne@chark.eu>
# License GPLv3 or later
#
# Remember to always use absolute path names!

from cda_parse import cdgoutparse

CdaSpec = cdgoutparse.CdaSpec
CdaWord = cdgoutparse.CdaWord

import os
import copy

import conllparser
import tree

from argparse import ArgumentParser

'''
Algorithm for generating prefixes with predictions
1. Determine needed words
   - all known words are needed
   - bottom up prediction: all words that are regents of needed words
     are needed (recursivly)
   - words that are dependents of needed words
     via a certain dependency type (label) are needed

2. Manage word ids
   - remove all unneded words
   - get the sequence of remaining word ids straight
   - update dependencies accordingly

3. write annotations
   - write known words, unchanged (besides possibly regent ids)
   - write virtual words
     - with extra atteribute "virtual"
     - with generic name
     - with changed other attributes?

'''

argp = ArgumentParser(description="Creates annotated prefixes with predictive"
                      " virtual nodes from whole-sentence annotations")
argp.add_argument("-v", "--verbose",
                  action="store_true", dest="verbose", default=False,
                  help="print more infos")
argp.add_argument("-d", "--dependents",
                  action="store_true", dest="dependents", default=False,
                  help="also keep dependents needed for valency reasons")
argp.add_argument("-r", "--restrict",
                  action="store_true", dest="restrict", default=False,
                  help="don't necessarily connect up to the root, "
                  "unless needed for connection reasons")
argp.add_argument("-s", "--strip",
                  action="store_true", dest="strip", default=False,
                  help="strip ALL virtual nodes "
                       "except the ones intruduced by padding")
argp.add_argument("-n", "--nonspec",
                  action="store_true", dest="nonspec", default=False,
                  help="Use nonspec instead of VNs")
argp.add_argument("-p", "--pad_unused",
                  action="store_true", dest="pad", default=False,
                  help="Pad with unused virtual nodes. Makes sure that "
                       "at least 2 VNoun and 1 VVerb is present, "
                       "also creates an unused node")
argp.add_argument("-k", "--keep-vn-identities",
                  action="store_true", dest="keepVNIdentities", default=False,
                  help="keep all lexical and morphosyntactic information for virtual nodes"
                  "an addition feature to mark them as virtual is added.")
argp.add_argument("-l", "--language", default="en",
                  help="Which language conventions to use, possible "
                  "values: de, en (default)")
argp.add_argument("inputtmpl", help="Template for the input files,"
                  " %%d will be replaced with the sentence number")
argp.add_argument("outputtmpl", help="Template for the output files,"
                  " %%d and %%d will be replaced with the"
                  " increment number and the sentence number")
argp.add_argument("startId", help="The first sentence to be processed",
                  type=int)
argp.add_argument("stopId", help="The last sentence to be processed",
                  type=int)

args = argp.parse_args()


if args.language == "de":
    import transform_de as transform
else:
    import transform_en as transform

cpos = transform.cpos
predictables = []
cond_predict = []
if args.dependents:
    predictables = transform.predictables
    cond_predict = transform.cond_predict

# TODO does this need i18n treatment?
tagsToKeep = ['cat', 'person', 'number', 'gender', 'case', 'oldpos']
tagsUnspecifiedValues = {'person': 'bot', 'number': 'bot', 'gender': 'bot'}


def activate(w, s):
    'set needed to true for the given word and all its transitive heads'
    tree.setNeeded(w, True)
    head = tree.getHeadOf(w, s, "SYN")
    if head and not tree.isNeeded(head):
        activate(head, s)


def activateRestricted(w, s):
    '''heads are marked first, before set to needed after calling this on
    all respective words, call activateRestrictedReverse on the whole
    sentence to finish the process
    '''
    head = tree.getHeadOf(w, s, "SYN")
    if head:
        if tree.isMarked(head) and not tree.isNeeded(head):
            tree.removeMarker(head)
            tree.setNeeded(head, True)
            #print "needs '" + head.word + "' via '" + w.word + "'"
        elif not tree.isMarked(head) and not tree.isNeeded(head):
            tree.setMarked(head, True)
            #print "marked '" + head.word + "' via '" + w.word + "'"
            activateRestricted(head, s)


def activateRestrictedReverse(w, s):
    '''if the word is needed make all marked children needed and continue
    recursively
    '''
    if not tree.isNeeded(w):
        return
    children = tree.getChildrenOf(w, s, "SYN")
    for child in children:
        if tree.isMarked(child) and not tree.isNeeded(child):
            #print "needs '" + child.word + "' via '" + w.word + "'"
            tree.removeMarker(child)
            tree.setNeeded(child, True)
            activateRestrictedReverse(child, s)


def pointerdec(s, i):
    '''decreases all pointers in s that are greater than i'''
    for w in s:
        if w.start > i:
            # huh? warum sollte oldpos reduziert werden?
            #tree.setFeat(w, "oldpos", str(int(tree.getFeat(w,"oldpos"))-1) )
            w.start -= 1
            w.end -= 1
        for k, v in w.specs.items():
            if v.type == 'dep':
                if v.pointer > i+1:
                    v.pointer -= 1


def orderVNsByCat(s, vsid):
    '''
    change the order of virtual nodes by sorting them by
    their PoS (the 'cat' feature) first
    and the earliest non-virtual dependent or regent second
    '''
    revmap1 = {}
    headmap = {}
    vs = []

    for w in s:
        headmap[w.start] = tree.getHead(w, "SYN") - 1

    #print headmap
    #print map(lambda x: tree.getFeat(x, "oldpos"), s)

    first_ref = [999 for x in s]
    for i in range(len(s)):
        head = headmap[i]
        if head < 0:
            continue
        first_ref[head] = min(first_ref[head], i)
        first_ref[i] = min(first_ref[i], head)

    for w in s:
        i = w.start
        if i < vsid:
            revmap1[i] = w
        else:
            cat = tree.getFeat(w, 'cat')
            #cat = ''
            #for k,v in w.specs.iteritems():
            #    if(k == 'cat'):
            #        cat=v.value
            #        break
            vs += [(w, i, cat, first_ref[i])]

    #print vsid
    #s_debug= "vs:"
    #for x,y,z in vs:
    #    s_debug += "(%s,%d,%s)"%(x.word, y, z)
    #print  s_debug

    vs_sorted = sorted(vs, key=lambda x: "%s%3d" % (x[2], x[3]))

    #s_debug= "vs_sorted:"
    #for x,y,z in vs_sorted:
    #    s_debug += "(%s,%d,%s)"%(x.word, y, z)
    #print  s_debug

    j = vsid
    for (w, i, cat, f_r) in vs_sorted:
        revmap1[j] = w
        j += 1

    return rearrangeIDs(s, revmap1)


def rearrangeIDs(s, revmap):
    '''
    Changes all ids in the sentence according to the given mapping.
    This includes the order of the words
    as well as all ids in dependency edges
    '''

    # init new sentence empty
    # will be filled later on with copies of the words in s
    new_s = []

    # we only have one mapping direction at hand: new_position->old
    # word but for changing the pointers we also need the other
    # direction: old_pos->new_pos

    # nonspec and nil are mapped to themselves, the rest follows later
    mapping = {-2: -2, -1: -1}
    for i in range(len(s)):
        w = revmap[i]
        mapping[w.start] = i  # 0 based, thus w.start instead of w.end
    for i in range(len(s)):
        # look up the word for the new position
        w = revmap[i]
        # and create a copy, specs will follow
        w2 = CdaWord(w.word, i, i+1, {})
        # copy specs, but pointers have to be mapped
        for k, v in w.specs.items():
            if(v.type == 'dep'):
                old_p = v.pointer
                p = mapping[old_p-1] + 1
                w2.specs[k] = CdaSpec('dep', k, v.value, p)
            elif(v.type == 'tag'):
                w2.specs[k] = CdaSpec('tag', k, v.value, v.pointer)
        new_s += [w2]
    return new_s


def virtualize(s, vsid):
    '''
    check whether words with index > vsid are necessary
    if no, they are removed (and ids changed accordingly)
    if yes, they are changed to generic virtual words
    '''

    sentencelength = len(s)
    if args.verbose:
        print("------------------------------------")
        if vsid < len(s):
            print("virtualizing from word '" + s[vsid].word + "' (" +
                  str(vsid) + ")")
        else:
            print("final increment")

    # now mark all words in the known prefix as needed
    # also mark all heads of needed words as needed
    if args.restrict:
        for i in range(vsid):
            tree.setNeeded(s[i], True)
        for i in range(vsid):
            activateRestricted(s[i], s)
        for w in s:
            activateRestrictedReverse(w, s)
    else:
        for i in range(vsid):
            activate(s[i], s)

    #for w in s:
    #    if tree.isNeeded(w):
    #        print w.word + " IS  needed"
    #    else:
    #        print w.word + " NOT needed!"

    # now it's time for the dependents
    # a word with a needed head will be activated, depending on the label
    change = True
    while change:
        change = False
        for w in s:
            if not tree.isNeeded(w):
                head = tree.getHeadOf(w, s, "SYN")
                label = tree.getLabel(w, "SYN")
                if head and tree.isNeeded(head) and label in predictables:
                    change = True
                    tree.setNeeded(w, True)
                if head and label in cond_predict and \
                   not tree.isVirtual(head, vsid):
                    change = True
                    tree.setNeeded(w, True)
    # apply transformation rules
    transform.applyAllRules(s, vsid)

    # remember old positions
    for w in s:
        w.specs["oldpos"] = CdaSpec("tag", "oldpos", str(w.start), 0)

    # replace head with nonspec, if restricted
    if args.restrict:
        for w in s:
            head = tree.getHeadOf(w, s, "SYN")
            if head and not tree.isNeeded(head):
                tree.setHead(w, "SYN", -1)
            head = tree.getHeadOf(w, s, "REF")
            if head and not tree.isNeeded(head):
                tree.setHead(w, "REF", 0)
                tree.setLabel(w, "REF", "")

    # we could have skipped the whole pre-processing if strip is set,
    # but this is the easier way.
    if args.strip or args.nonspec:
        for i in range(vsid):
            if tree.getHead(s[i], "SYN") >= vsid:
                tree.setHead(s[i], "SYN", -1)
        for i in range(vsid, len(s)):
            tree.setNeeded(s[i], False)

    if args.nonspec and vsid < sentencelength:
        nonspecNode = CdaWord("__NONSPEC__", len(s), len(s)+1, {})
        s.append(nonspecNode)
        nonspecNode.specs["SYN"] = CdaSpec("dep", "SYN", "NONSPEC", 0)
        nonspecNode.specs["cat"] = CdaSpec("tag", "cat", "NONSPEC", 0)
        tree.setNeeded(nonspecNode, True)
        for i in range(vsid):
            if tree.getHead(s[i], "SYN") == -1:
                tree.setHead(s[i], "SYN", len(s))

    # now remove unused words and clean up ids
    change = True
    while change:
        change = False
        for i in range(len(s)):
            if not tree.isNeeded(s[i]):
                del s[i]
                pointerdec(s, i)
                change = True
                break

    #print map(lambda x: tree.isNeeded(x), s)
    #print map(lambda x: tree.getHead(x,"SYN"), s)

    for i in range(vsid, len(s)-args.nonspec):
        #don't touch nonspec if we have it
        w = s[i]
        if args.keepVNIdentities:
            w.specs = w.specs.copy()
            w.specs["virtual"] = CdaSpec("tag", "virtual", "true", 0)
        else:
            transform.word2VN(w, tagsToKeep, tagsUnspecifiedValues)

    # the needed flag was marked directly in the words,
    # remove these flags now, we don't want them in the final output
    for w in s:
        tree.removeNeededMarker(w)
        tree.removeMarker(w)

    if args.pad and vsid < sentencelength:
        transform.pad_with_unused_vns(s)

    return orderVNsByCat(s, vsid)


def write_prefixes(s0, fnametpl, sentencename):
    '''writes prefixes for given sentence s. fnametpl is a template
    for the output files (e.g. "/path/sentence-%i.cda"). pointers
    pointing to the future are set to -1'''
    if args.verbose:
        tree.printsen(s0)
        print("length: " + str(len(s0)))
    # no increment ID in the template -> put everything in the same file
    multifile = fnametpl.count(r'%') == 0
    for i in range(len(s0)):
        s = copy.deepcopy(s0)
        filename = fnametpl
        if not multifile:
            filename = fnametpl % (sentencename, i+1)
        mode = 'a' if multifile else 'w'
        f = open(filename, mode)
        sentenceid, suffix = os.path.basename(filename).rsplit(".", 1)
        try:
            s = virtualize(s, i+1)
        except:
            tree.printsen(s0)
            exit(1)
        if args.verbose:
            tree.printsen(s)
        else:
            pass
            #print(sentenceid + ":" + sentenceid)

        # write in conll-X format if wanted by the user
        if suffix == "conll":
            for w in s:
                # id word lemma cpos pos feats head deprel phead pdeprel
                # id word   -   cpos pos   -   head deprel   -      -
                feats = w.specs.copy()
                feats.pop("SYN")
                feats.pop("cat")
                feats.pop("oldpos")
                if "virtual" in w.specs:
                    feats["virtual"] = CdaSpec("tag", "virtual", "true", 0)
                if len(feats) == 0:
                    featsstr = "-"
                else:
                    try:
                        featsstr = "|".join([k+"="+str(v.value) for (k,v) in feats.items()])
                    except:
                        print(feats)
                        exit(1)
                f.write("%s\t%s\t_\t%s\t%s\t%s\t%s\t%s\t_\t_\n"
                        % (w.end, w.word, cpos(w.specs["cat"].value),
                           w.specs["cat"].value, featsstr,
                           w.specs["SYN"].pointer, w.specs["SYN"].value))
            if multifile:
                f.write("\n")
            f.close()
            continue

        f.write("'%s' : '%s' <->\n" % (sentenceid, sentenceid))
        for j in range(len(s)):
            if j > 0:  # no comma before the first word
                f.write(",\n")
            w = s[j]  # word
            f.write(" %s %s '%s'\n" % (w.start, w.end,
                                       w.word.replace("'", r"\'")))
            for k, v in w.specs.items():
                if v.type == "tag":
                    f.write("'%s' / '%s'\n" % (str(v.key),
                                               str(v.value).replace("'",
                                                                    "\\'")))
            for k, v in w.specs.items():
                if v.type != "tag":
                    pointer = v.pointer
                    f.write("'%s' -> '%s' -> %i\n" % (v.key, v.value, pointer))
        f.write(";\n")
        f.close()


# just read the whole file
if args.inputtmpl.count(r'%') == 0:
    sname = os.path.basename(args.inputtmpl).rsplit(".", 1)[0]  # strip the file ending (.cda)
    f = open(args.inputtmpl)
    sentencestr = ""
    for line in f:
        sentencestr += line
        if line == "\n":
            if len(sentencestr) > 10:
                sentence = conllparser.parse(sentencestr)
                write_prefixes(sentence, args.outputtmpl, sname)
            sentencestr = ""

    exit()
    
        
for i in range(args.startId, args.stopId+1):
    inname = args.inputtmpl % (i)
    print(inname)
    f = open(inname)
    if inname.endswith("conll"):
        sentence = conllparser.parse(f.read())
    else:
        sentence = cdgoutparse.parser.parse(f.read())
    sname = os.path.basename(inname).rsplit(".", 1)[0]  # strip the file ending (.cda)
    print(sname)
    write_prefixes(sentence, args.outputtmpl, sname)
