from cda_parse.cdgoutparse import CdaSpec, CdaWord


def parse(conllstr):
    '''parses a sentence from a conll-X style string'''
    result = []
    for line in conllstr.split("\n"):
        tmp = line.split("\t")
        if len(tmp) < 8 or not tmp[0].isdigit():  # not a valid line; ignore
            continue
        specs = {"SYN": CdaSpec("dep", "SYN", tmp[7], int(tmp[6]))}
        specs["cat"] = CdaSpec("tag", "cat", tmp[4], 0)
        if ("|" in tmp[5]):
            for (k,v) in [x.split("=") for x in tmp[5].split("|")]:
                specs[k] = CdaSpec("tag", k, v, 0)
        if tmp[1] == "[virtual]":
            specs["virtual"] = CdaSpec("tag", "virtual", "true", 0)
        result.append(CdaWord(tmp[1], int(tmp[0])-1, int(tmp[0]), specs))
    return result
