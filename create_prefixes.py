#! /usr/bin/env python
# -*- coding:utf-8 -*-
# Author: Arne Köhn <arne@arne-koehn.de>
# License GPLv3 or later
# usage: create_prefixes.py /path-to-out/%s-%i.cda /path/sentence1.cda ...
# %s is the original file name, %i the increment. %s has to be before %i.
# always remember to use absolute path names!

from cda_parse import cdgoutparse

import sys, os

def write_prefixes(s, fnametpl, sentencename):
    '''writes prefixes for given sentence s. fnametpl is a template
    for the output files (e.g. "/path/sentence-%i.cda"). pointers
    pointing to the future are set to -1'''
    for i in xrange(len(s)-2):
        filename = fnametpl % (sentencename, i) 
        f = open(filename, 'w')
        sentenceid = os.path.basename(filename)[:-4]
        f.write("'%s' : '%s' <->\n"%(sentenceid, sentenceid))
        for j in xrange(i+1):
            if j>0: # no comma before the first word
                f.write(",\n")
            w = s[j] # word
            f.write(" %s %s '%s'\n" % (w.start, w.end, w.word.replace("'",r"\'")))
            for k,v in w.specs.iteritems():
                if v.type == "tag":
                    f.write("'%s' / '%s'\n"%(str(v.key), str(v.value)))
                else:
                    pointer = v.pointer
                    if v.pointer > i+1:
                        pointer = -1
                    f.write("'%s' -> '%s' -> %i\n"%(v.key, v.value, pointer))
        f.write(";\n")
        f.close()
    

outdirtmpl = sys.argv[1]
for i in sys.argv[2:]:
    f = open(i)
    sentence = cdgoutparse.parser.parse(f.read())
    sname = os.path.basename(i)[:-4] # strip the file ending (.cda)
    write_prefixes(sentence, outdirtmpl, sname)

