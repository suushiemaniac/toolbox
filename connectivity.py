#! /usr/bin/env python
# -*- coding:utf-8 -*-
# An evaluator for cda files without good documentation
# Copyright (C) 2010  Niels Beuck
# Copyright (C) 2010  Arne Köhn <arne@arne-koehn.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# call example: 
# ./connectivity.py '../negra/negra-s(1).cda' 1 500


from cda_parse.cdgoutparse import parser, CdaSpec, CdaWord, ParseException

import sys
import copy
import traceback
import os
import tree

from optparse import OptionParser

optparser = OptionParser()
optparser.add_option("-v", "--verbose",
                  action="store_true", dest="verbose", default=False,
                  help="print more info when processing")
optparser.add_option("-n", "--newonly",
                  action="store_true", dest="newOnly", default=False,
                  help="instead of all used VNs only count those new in the increment")

(options, args) = optparser.parse_args()
verbose        = options.verbose
newOnly        = options.newOnly

#TODO
if len(args) == 0:
    print """usage: connectivity path filepattern minId maxId"""
    exit
if (len(args)) != 4:
    print """you must specify 4 parameters! """
    exit

path    = str( args[0])
pattern = str( args[1])
minId   = int( args[2])
maxId   = int( args[3])


# print all dependency edges in the annotation
def printAnno(anno):
    i=1
    for word in anno:
        print i, word.word, "--", word.specs["SYN"].value,"->", word.specs["SYN"].pointer
        i+=1

def readFile(name):
    return parser.parse(unicode(open( name ).read(), 'latin-1'))

def evalSentence(gold, labels, poss, lps, lls, pgs, amounts, conLen, id):
    oldIsNeeded = len(gold)*[0]
    for i in xrange(len(gold)):
        isNeeded     = evalVNsNeeded(gold, i, labels, poss, lps, lls, pgs, id)
        cl = evalConnectionPath(gold, i, oldIsNeeded)
        if(cl>3):
             print "cn-len of %d in %d of %d"%(cl,i, id)
        conLen[cl] += 1
        n= reduce(lambda x, y: x+y, isNeeded[i+1:], 0)
        amounts[n]+=1
        oldIsNeeded = isNeeded

def sortedKeys(dic, by):
    keys= dic.keys()
    keys.sort(by)
    return keys

def printSorted(dic, n):
    dKeys = dic.keys()
    dKeys.sort( lambda x,y: cmp(dic[y], dic[x]) )
    i=0
    for key in dKeys:
        print "%10s: %5d"%(key, dict[key])
        i+=1
        if n>0 and i==n:
            break

def checkVNvariants(variantCounts, cpc):
    for variantName in vnVariants:
        variant = vnVariants[variantName]
        fits = True
        for cPoS in cpc:
            if cpc[cPoS] > variant[cPoS]:
                fits= False
                break
        if fits:
            variantCounts[variantName] += 1



##############
#### main ####
##############

labels  = {}
poss    = ['ADJA','ADJD','ADV','APPR','APPRART','APPO','APZR','ART','CARD','FM','ITJ','ORD','KOUI','KOUS','KON','KOKOM','NN','NE','PDS','PDAT','PIS','PIAT','PIDAT','PPER','PPOSS','PPOSAT','PRELS','PRELAT','PRF','PROAV','PWS','PWAT','PWAV','PAV','PTKZU','PTKNEG','PTKVZ','PTKANT','PTKA','SGML','SPELL','TRUNC','VVFIN','VVIMP','VVINF','VVIZU','VVPP','VAFIN','VAIMP','VAINF','VAPP','VMFIN','VMINF','VMPP','XY','\$,','\$.','\$(']

#'ADJA','ADJA','ADJD','ADV','APPR','APPRART','APPO','APZR','ART','CARD',,'ITJ','ORD','KOUI','KOUS','KON','KOKOM','PDS','PDAT','PIS','PIAT','PIDAT','PPER','PPOSS','PPOSAT','PRELS','PRELAT','PRF','PWS','PWAT','PWAV','PAV','PTKZU','PTKNEG','PTKVZ','PTKANT','PTKA','SGML','SPELL','TRUNC',,'XY','\$,','\$.','\$('
coarsePoss = {'kon':['KON'],'zu':['PTKZU'],'nominals':['NN','NE','FM','PPER','PDS','PIS','PRF','PWS','TRUNC'], 'verbs':['VVFIN','VVIMP','VVINF','VVIZU','VVPP','VAFIN','VAIMP','VAINF','VAPP','VMFIN','VMINF','VMPP'], 'adjectives':['ADJA','ADJD'], 'adverbs':['ADV'], 'PP':['APPR','APPRART'], 'rel-pro':['PRELS','PRELAT'],'misc':['APPO','APZR','ART','CARD','ITJ','ORD','KOUI','KOUS','KOKOM','PDAT','PIAT','PIDAT','PPOSS','PPOSAT','PROAV','PWAT','PWAV','PAV','PTKNEG','PTKVZ','PTKANT','PTKA','SGML','SPELL','XY','\$,','\$.','\$('] }
#coarsePoss = {'kon':['KON'],'zu':['PTKZU'],'nominals':['NN','NE','FM','PPER','PDS','PIS','PRF','PWS','TRUNC'], 'verbs':['VVFIN','VVIMP','VVINF','VVIZU','VVPP','VAFIN','VAIMP','VAINF','VAPP','VMFIN','VMINF','VMPP'], 'adjectives':['ADJA','ADJD'], 'misc':['ADV','APPR','APPRART','PRELS','PRELAT','APPO','APZR','ART','CARD','ITJ','ORD','KOUI','KOUS','KOKOM','PDAT','PIAT','PIDAT','PPOSS','PPOSAT','PROAV','PWAT','PWAV','PAV','PTKNEG','PTKVZ','PTKANT','PTKA','SGML','SPELL','XY','\$,','\$.','\$('] }

noVNs={'nominals':0,'verbs':0,'adjectives':0,'adverbs':0,'PP':0,'rel-pro':0,'kon':0,'zu':0,'misc':0}
nnv  ={'nominals':2,'verbs':1,'adjectives':0,'adverbs':0,'PP':0,'rel-pro':0,'kon':0,'zu':0,'misc':0}
nnva ={'nominals':2,'verbs':1,'adjectives':1,'adverbs':0,'PP':0,'rel-pro':0,'kon':0,'zu':0,'misc':0}
nnvva={'nominals':2,'verbs':2,'adjectives':1,'adverbs':0,'PP':0,'rel-pro':0,'kon':0,'zu':0,'misc':0}
nnvvac={'nominals':2,'verbs':2,'adjectives':1,'adverbs':0,'PP':0,'rel-pro':0,'kon':1,'zu':0,'misc':0}
nnvvacp={'nominals':2,'verbs':2,'adjectives':1,'adverbs':0,'PP':1,'rel-pro':0,'kon':1,'zu':0,'misc':0}
nnvvacpav={'nominals':2,'verbs':2,'adjectives':1,'adverbs':1,'PP':1,'rel-pro':0,'kon':1,'zu':0,'misc':0}

vnVariants={"nnv":nnv,"noVNs":noVNs,"nnva":nnva,"nnvva":nnvva, "nnvvacp":nnvvacp, "nnvvac":nnvvac,"nnvvacpav":nnvvacpav}
vnVariantCount={"nnv":0,"noVNs":0,"nnva":0,"nnvva":0,"nnvvac":0,"nnvvacp":0,"nnvvacpav":0}

# from generate prefixes, needed for deciding who predicted a VN
predictables = ['SUBJ', 'SUBJC', 'PN', 'CJ']
cond_predict = ['OBJA', 'OBJD', 'OBJC', 'PRED','OBJP', 'AUX', 'PART','S']


lps     = {}
lls     = {}

pgs     = {}
pgs["nominal"]  =[0]*6
pgs["verb"]     =[0]*6
pgs["adjective"]=[0]*6
pgs["misc"]     =[0]*6

perPoS = {}
for p in poss:
    perPoS[p]= [0]*10

perPoSCoarse= {}
for c in coarsePoss:
    perPoSCoarse[c]=[0]*10

amounts = [0]*12
conLen  = [0]*10


# ling-cat stuff
vnTotal = 0
buPreds = {}
tdPreds = {}

##

# what we want to determine in this skript:
# I.   Distribution of the number of of overall VNs needed in a PDA
# II.  Distribution of the number of VNs per part of speech (coarse, fine grained)
# III. Distribution of the number of VNs in the connection path, i.e. those new in the increment

files= os.listdir(path)

for i in xrange(minId, maxId+1):
    
    j = 0
    ok= True
    prevSen= None
    
    while ok:
        fName = pattern%(i,j)
        j+=1
        if not fName in files:
            ok = False
            break
        
        sen = readFile(path+fName)
        
        n = 0
        posNs = {}
        for p in poss:
            posNs[p]=0
        
        cPosNs= {}
        for c in coarsePoss:
            cPosNs[c]=0
        
        for w in sen:
            if tree.isVirtualByFeat(w):
                if newOnly:
                    if not prevSen:
                        break
                    oldposition= tree.getFeat(w, "oldpos")
                    #print "old pos:" + oldposition
                    found = False
                    
                    for w2 in prevSen:
                        if tree.checkFeat(w2, "oldpos", oldposition):
                            found = True
                            break
                    
                    if found:
                        continue
                
                n += 1
                pos = tree.getFeat(w, 'cat')
                posNs[pos] += 1
                cpos='misc'
                for c in coarsePoss:
                    if pos in coarsePoss[c]:
                        cpos = c
                        break
                cPosNs[cpos]+=1

                # determining who predicted a VN
                vnTotal +=1
                botUpPred = tree.findBuPredictor(w, sen)
                head  = tree.getHeadOf(w, sen, 'SYN')
                label = tree.getLabel(w,'SYN')
                couldHaveBeenPredictedTopDown = head and (label in predictables or not tree.isVirtualByFeat(head) and label in cond_predict)
                # there are even more conditional top down predictors that we don't cover here (TRUNC and KON constructions, see tree.py)
                # but those are typically childless, and will end up in the else clause below (where couldHaveBeen... is not checked again)

                # if the VN has a child and could have been predicted top down, the leftmost word wins as predictor
                if botUpPred and (not couldHaveBeenPredictedTopDown or botUpPred.end < head.end):
                    buLabel = tree.getLabel(botUpPred,'SYN')
                    if not buLabel in buPreds:
                        buPreds[buLabel]=0
                    buPreds[buLabel]+=1
                else:
                    if not label in tdPreds:
                        tdPreds[label]=0
                    tdPreds[label]+=1
                
        amounts[n] += 1
        for p in poss:
            pn = posNs[p]
            perPoS[p][pn]+=1
        
        # alarms
        #for p in poss:
        #    pn = posNs[p]
        #    if p=="APPR" and pn == 1:
        #        print "APPR in "+fName

        for c in coarsePoss:
            cpn = cPosNs[c]
            #if c == "verbs" and cpn == 4:
            #    print fName
            perPoSCoarse[c][cpn]+=1

        checkVNvariants(vnVariantCount, cPosNs)

        prevSen=sen
        
print "amounts:"
print amounts
print ""

#print "per PoS:"
#print "%7s|%5d |%5d |%5d |%5d |%5d |%5d |"%('name', 0, 1, 2, 3, 4, 5)
#print "-------------------------------------------------------------------------------"
#keys= sortedKeys(perPoS, lambda x,y: -cmp(perPoS[x][1],perPoS[y][1]))
#for k in keys:
#    print "%7s:%5d ,%5d ,%5d ,%5d ,%5d ,%5d"%(k,perPoS[k][0],perPoS[k][1],perPoS[k][2],perPoS[k][3],perPoS[k][4],perPoS[k][5])

print ""
print "per coarse PoS :"
print "%10s|%5d |%5d |%5d |%5d |%5d |%5d |"%('name', 0, 1, 2, 3, 4, 5)
print "-------------------------------------------------------------------------------"
keys= sortedKeys(perPoSCoarse, lambda x,y: -cmp(perPoSCoarse[x][1],perPoSCoarse[y][1]))
for k in keys:
    print "%10s:%5d ,%5d ,%5d ,%5d ,%5d ,%5d "%(k,perPoSCoarse[k][0],perPoSCoarse[k][1],perPoSCoarse[k][2],perPoSCoarse[k][3],perPoSCoarse[k][4],perPoSCoarse[k][5])

print ""
print "VN sets:"
for variant in vnVariants:
    print "%s : %d"%(variant,vnVariantCount[variant])

print "======================================================================"
print "ling-cats"
print "bottom-up:"
for label in sortedKeys(buPreds, lambda x,y: -cmp(buPreds[x], buPreds[y])):
    print "%5s : %.2f"%(label, buPreds[label]*100.0/vnTotal)
print ""
print "top-down:"
for label in sortedKeys(tdPreds, lambda x,y: -cmp(tdPreds[x], tdPreds[y])):
    print "%5s : %.2f"%(label, tdPreds[label]*100.0/vnTotal)
