#! /usr/bin/env python3
# -*- coding:utf-8 -*-
# A script to convert 1-n cda files into a conll file
# Copyright (C) 2011  Niels Beuck
# Copyright (C) 2011-2015  Arne Köhn

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# #####################################################################

# This is a converter for dependency analyses
# - reads .cda files in the dependency structure format as used by WCDG and defined in [Foth06]
# - writes .conll files as used in the CoNLL-X shared tast [buchholzMarsi06]
#
# In constrast to the original CoNLL-data for German,
# the coarse PoS tags are used (e.g., 'V' for all verbal PoS tags),
# i.e. the two PoS tag fields might actually differ

# WCDG:    http://nats-www.informatik.uni-hamburg.de/view/CDG
# CoNLL-X: http://www.clips.ua.ac.be/conll2006/
#
# [Foth06]: Kilian Foth, 2006. "Eine umfassende
# Constraint-Dependenz-Grammatik desDeutschen". Manual, Universität
# Hamburg.

# [buchholzMarsi06]: Buchholz, S. & Marsi, E. CoNLL-X shared task on
# Multilingual Dependency Parsing Proceedings of CoNLL-X, 2006

# call example:
# cda_parse.cdgoutparse "cdatest/1-depling2011nonspec-(1).cda" test2.conll 0 5


from cda_parse.cdgoutparse import parser

import os

import codecs

from optparse import OptionParser

optparser = OptionParser()
optparser.add_option("-v", "--verbose",
                     action="store_true", dest="verbose", default=False,
                     help="print more information while processing")
#optparser.add_option("-f", "--file",
#                  action="store", dest="filename",
#                  help="""name of the output file,
#if no filename is given,
#output is written to stdout""")
optparser.add_option("-d", "--dir", action="store_true", dest="convertdir",
                     help="Read cda files from a directory "
                     "and write conll files to a directory")
optparser.add_option("-r", "--range",
                     action="store_true", dest="useRange", default=False,
                     help="Use this flag to read a sequence of input files "
                     "instead of a single one."
                     "The output will still be written in one file."
                     "The filename has to contain the placeholder %id for the "
                     "id and you have to give start and end id as additional "
                     "arguments, e.g.: convert_cda2conll.py -r \"foo-%id.cda\""
                     " bar.conll 10 20")

(options, args) = optparser.parse_args()
verbose = options.verbose
useRange = options.useRange

if len(args) == 0:
    print("""usage:
  convert_cda2conll.py [in].cda [out].conll
  or
  convert_cda2conll.py -r "[in]%id.cda" [out].conll [nmin] [nmax]
  use -h for more options""")
#  convert_cda2conll.py -f [output].conll [name].cda
    exit()
if not useRange and (len(args)) != 2:
    print("""you must specify 2 parameters! """)
    exit
if useRange and (len(args)) != 4:
    print("""you must specify 4 parameters is you use the --range option! """)
    exit


def printAnno(anno):
    """print all dependency edges in the annotation"""
    i = 1
    for word in anno:
        print(i, word.word, "--", word.specs["SYN"].value, "->",
              word.specs["SYN"].pointer)
        i += 1


def readCda(cdaPattern, id):
    """returns a cda parse, read from a file identified by pattern and id"""
    #print "pattern " + gPattern
    name = cdaPattern.replace("%id", str(id))
    #print "opening " + name
    return readCdaFile(name)


def readCdaFile(name):
    """returns a cda parse, read from a file of the given name"""
    try:
        return parser.parse(open(name).read())
    except:
        print("couldn't parse", name)
        exit(1)

# conll format
# 0 id
# 1 Form
# 2 Lemma
# 3 PoS (coarse)
# 4 PoS STTS
# 5 features
# 6 Head
# 7 Label
# 8 projective head (not available in our case)
# 9 projective Label (not available in our case) 

def makeConllTable(cda):
    table = []
    i = 1
    for word in cda:
        #print word.word
        line = []
        line.append("%i" % i)
        line.append(word.word)
        lemma = word.specs["base"].value if "base" in word.specs else word.word
        line.append(lemma)
        line.append(makeCoarse(word.specs["cat"].value))
        line.append(word.specs["cat"].value)
        feats = []
        for k, v in word.specs.items():
            if k in ('SYN', 'REF','cat', 'base'):
                # those are not features, ignore
                continue
            feats.append(k+'='+v.value)
        if len(feats) > 0:
            line.append("|".join(feats))
        else:
            line.append("_")
        line.append("%i" % word.specs["SYN"].pointer)
        label = word.specs["SYN"].value
        if label == '':
            line.append("ROOT")
        else:
            line.append(label)
        # phead and plabel are empty
        line.append("_")
        line.append("_")
        i += 1
        #print "line"
        #print line
        table.append(line)

    return table


def writeOutput(lines, filename):
    f = codecs.open(filename, 'w', encoding='utf-8')
    for line in lines:
        f.write(line)
    f.close()


def makeOutput(table):
    lines = []
    for line in table:
        s = ""
        start = True
        for entry in line:
            if not start:
                s += "\t"
            else:
                start = False
            if entry != "":
                s += entry
            else:
                s += "_"

        #print type(s)
        #sys.stdout.write(s+"\n")
        lines += [s + "\n"]
    lines += ["\n"]
    return lines


def makeCoarse(tag):
    if tag in ["NE", "NN"]:
        return "N"
    if tag in ["VVFIN", "VVINF", "VVPP", "VVIZU", "VVIMP", "VAFIN", "VAINF",
               "VAPP", "VAIMP", "VMFIN", "VMINF", "VMPP"]:
        return "V"
    if tag in ["APPR", "APPRART", "APPO"]:
        return "PREP"
    if tag in ["PPOSAT", "ART", "PWAT", "PRELAT", "PPOSAT", "PIAT",
               "PIDAT", "PDAT"]:
        return "ART"
    if tag in ["PWS", "PRF", "PRELS", "PPOSS", "PPER", "PIS", "PDS"]:
        return "PRO"
    if tag in ["ADV", "ADJD"]:
        return "ADV"
    else:
        return tag

    # other PoS-Tags:
    # APZR, PAV

outfilename = args[1]

if useRange:
    inpattern = args[0]
    output = []
    for i in range(int(args[2]), int(args[3])+1):
        cda = readCda(inpattern, i)
        try:
            table = makeConllTable(cda)
        except:
            print("makeconll failed at", i)
            continue
        output += makeOutput(table)
    writeOutput(output, outfilename)
elif options.convertdir:
    indir = args[0]
    outdir = args[1]
    infiles = os.listdir(indir)
    for f in infiles:
        cda = readCdaFile(indir+"/"+f)
        table = makeConllTable(cda)
        writeOutput(makeOutput(table),
                    outdir+"/"+os.path.splitext(f)[0]+".conll")
else:
    infilename = args[0]
    cda = readCdaFile(infilename)
    table = makeConllTable(cda)
    writeOutput(makeOutput(table), outfilename)
