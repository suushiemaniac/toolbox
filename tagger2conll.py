#! /usr/bin/env python
# -*- coding:utf-8 -*-
# author: Arne Köhn <arne@arne-koehn.de>
# license: PD, GPL3+, MIT... as you wish.

# this scripts takes tagged sentences (word\ttag) and gives conll-blind-style tagged sentences.

import re,sys

r = re.compile(r'([^\t]*)\t(.*)')

def cpos_de(tag):
    if tag in ["NE", "NN"]:
        return "N"
    if tag in ["VVFIN", "VVINF", "VVPP", "VVIZU", "VVIMP",
               "VAFIN", "VAINF", "VAPP", "VAIMP", "VMFIN",
               "VMINF", "VMPP"]:
        return "V"
    if tag in ["APPR", "APPRART", "APPO"]:
        return "PREP"
    if tag in ["PPOSAT", "ART", "PWAT", "PRELAT", "PPOSAT", "PIAT",
               "PIDAT", "PDAT"]:
        return "ART"
    if tag in ["PWS", "PRF", "PRELS", "PPOSS", "PPER", "PIS", "PDS"]:
        return "PRO"
    if tag in ["ADV", "ADJD"]:
        return "ADV"
    else:
        return tag

def cpos_en(tag):
    if (tag == "PRP" or tag == "PRP$"):
        return tag
    return tag[:2]

cpos = cpos_de
try:
    if sys.argv[1] == "en":
        cpos = cpos_en
except:
    pass

nr = 1
for l in sys.stdin:
    if l == "\n":
        nr = 1
        print
        continue
    word, tag = r.match(l).groups()
    print "%(nr)i\t%(word)s\t_\t%(ctag)s\t%(tag)s\t_\t_\t_\t_\t_" % {'nr':nr, 'word':word, 'ctag':cpos(tag), 'tag':tag}
    nr += 1
